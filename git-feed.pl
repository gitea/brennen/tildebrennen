#!/usr/bin/env perl

# TODO: this really just ought to be a general-purpose utility

use warnings;
use strict;
use 5.10.0;
use XML::Atom::SimpleFeed;
use HTML::Entities;

my $page_url = "http://tilde.club/~brennen/";
my $feed_url = "http://tilde.club/~brennen/feed.xml";

my $log = `git log -10 --pretty=format:"%H _ %ai _ %s"`;

my $feed = XML::Atom::SimpleFeed->new(
  title     => "Brennen's tilde.club page",
  link      => $page_url,
  link      => { rel => 'self', href => $feed_url, },
  author    => 'Brennen Bearnes',
  id        => $page_url,
  generator => 'XML::Atom::SimpleFeed',
  # updated   => iso_date(Wala::get_mtime($month_file)),
);

while ($log =~ m/^([a-z0-9]+) _ (.*) _ (.*)$/gm) {
  my $hash = $1;
  my $full_commit = `git show $hash`;
  my $formatted_commit = '<pre>' . encode_entities($full_commit) . '</pre>';
  my $date = $2;
  my $subj = $3;

  $feed->add_entry(
    title     => $subj,
    link      => $page_url,
    id        => $hash,
    content   => $formatted_commit,
    updated   => $date,
  );
}

print $feed->as_string;

sub iso_date {
  my ($time) = @_;
  return strftime("%Y-%m-%dT%H:%M:%SZ", localtime($time));
}
